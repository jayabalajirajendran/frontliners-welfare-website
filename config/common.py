import binascii
import hashlib
import os
import random
import string

# Settings.py Configuration

# Settings.py Path
SETTINGS_PATH = 'config.settings'

# Template Directory
TEMPLATE_DIRECTORY = '/Users/jb/LocalDisk/DBS/Semester 2/Web Development (Obinna)/DBS-Welfare-App/webapp'

# MYSQL Configuration
DATABASE_NAME = 'Welfare'
DATABASE_USERNAME = 'root'
DATABASE_PASSWORD = 'jayabalaji'
DATABASE_HOST = 'localhost'
DATABASE_PORT = '3306'

# API Configuration

# News API Key
API_KEY = '5237fbce077647e88932abcaebcb43af'
NEWS_SOURCE = 'the-irish-times'

# Stubbed JSON Resource Folder
RESOURCE_FOLDER = '/Users/jb/LocalDisk/DBS/Semester 2/Web Development (Obinna)/DBS-Welfare-App/resources/'

# Fetch from API
FETCH_FROM_API = True


# Verify the USER Password
def verify_password(password, database_password):
    salt = password[:64]
    stored_password = password[64:]
    databasePassword_hash = hashlib.pbkdf2_hmac('sha512',
                                                database_password.encode('utf-8'),
                                                salt.encode('ascii'),
                                                100000)
    databasePassword_hash = binascii.hexlify(databasePassword_hash).decode('ascii')
    return databasePassword_hash == stored_password


# Hash the User Password to store in DATABASE
def hash_password(password):
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    hashPassword = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                       salt, 100000)
    hashPassword = binascii.hexlify(hashPassword)
    return (salt + hashPassword).decode('ascii')


# Random generator Coupon Code
def get_random_alphanumeric_string(length):
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(length)))
    return result_str.upper()
