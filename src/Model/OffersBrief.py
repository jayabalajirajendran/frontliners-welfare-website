class OffersBrief:
    def __init__(self, storeId, storeName, storeType, storeInCharge, description, image, storePara, storeMap):
        self.id = storeId
        self.storeName = storeName
        self.storeType = storeType
        self.storeInCharge = storeInCharge
        self.description = description
        self.image = image
        self.storePara = storePara
        self.storeMap = storeMap

    def getStoreId(self):
        return self.id

    def getStoreType(self):
        return self.storeType

    def getStoreName(self):
        return self.storeName

    def getStoreInCharge(self):
        return self.storeInCharge

    def getDescription(self):
        return self.description

    def getImage(self):
        return self.image

    def getStorePara(self):
        return self.storePara

    def getStoreMap(self):
        return self.storeMap
