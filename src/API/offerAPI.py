from django.shortcuts import render, redirect
from django.contrib import messages
from src.Model.OffersBrief import OffersBrief
from config import common
from webapp import forms
from webapp import models
from src.serializers import serializer
from rest_framework.utils import json
from datetime import datetime


def offers(request):
    try:
        data = []
        offersDictionary = {}
        if request.session.has_key('logged_in'):
            offersDictionary['logged_in'] = "true"
        elif request.session.has_key('superAdmin'):
            offersDictionary["superAdmin"] = "true"
        offerDetails = models.Offers.objects.all()
        offerDataSerializer = serializer.OfferSerializer(offerDetails, many=True)
        offer_data = json.loads(json.dumps(offerDataSerializer.data))
        for offerBrief in offer_data:
            offersObject = OffersBrief(offerBrief['id'], offerBrief['StoreName'], [offerBrief['StoreType']],
                                       offerBrief['StoreInCharge'], offerBrief['Description'],
                                       offerBrief['Image'], offerBrief['StorePara'], offerBrief['StoreMap'])
            data.append(offersObject.__dict__)
        offersDictionary['offersList'] = data
        return render(request, "offers.html", {'data': offersDictionary})
    except:
        messages.error(request, "Something went wrong")
        return redirect("/")


def storeBrief(request):
    try:
        offersDictionary = {}
        storeId = request.GET.get('id')
        if request.session.has_key('logged_in'):
            offersDictionary['logged_in'] = "true"
        elif request.session.has_key('superAdmin'):
            offersDictionary["superAdmin"] = "true"
        offerDetails = models.Offers.objects.all()
        offerDataSerializer = serializer.OfferSerializer(offerDetails, many=True)
        offer_data = json.loads(json.dumps(offerDataSerializer.data))
        for offerBrief in offer_data:
            offersObject = OffersBrief(offerBrief['id'], offerBrief['StoreName'], [offerBrief['StoreType']],
                                       offerBrief['StoreInCharge'], offerBrief['Description'],
                                       offerBrief['Image'], offerBrief['StorePara'], offerBrief['StoreMap'])
            if storeId == str(offersObject.getStoreId()):
                offersDictionary['offersList'] = offersObject.__dict__
        return render(request, 'storeBrief.html',
                      {'data': offersDictionary})
    except:
        messages.error(request, "Something went wrong, Please try again later")
        return redirect("/")


def couponOffer(request):
    try:
        form = forms.UpdateStoreForm(request.POST)
        couponId = form.data.get("submit")
        if couponId == 'cancel':
            return redirect("/")
        if couponId == 'logIn':
            return redirect("/login")
        username = request.session.get('userName')
        userDetails = models.User.objects.get(UserName=username)
        couponDetails = list(models.User.objects.filter(UserName=username).values_list('Coupons', flat=True))
        if couponDetails[0] is not None:
            couponDetailsList = couponDetails
        else:
            couponDetailsList = None
        userDataSerializer = serializer.UserSerializer(userDetails)
        userData = json.loads(json.dumps(userDataSerializer.data))
        if couponDetailsList is not None:
            userCoupon = json.loads(userData['Coupons'])
            for coupons in userCoupon:
                if coupons['id'] == int(couponId):
                    couponDictionary = coupons
                    if request.session.has_key('logged_in'):
                        couponDictionary['logged_in'] = "true"
                    elif request.session.has_key('superAdmin'):
                        couponDictionary["superAdmin"] = "true"
                    messages.info(request, "You already claimed the coupon code")
                    return render(request, 'couponOffer.html',
                                  {'data': couponDictionary})
        if couponDetailsList is not None:
            couponList = json.loads(userData['Coupons'])
        else:
            couponList = []
        couponCode = common.get_random_alphanumeric_string(8)
        offerDetails = models.Offers.objects.get(id=couponId)
        offerDataSerializer = serializer.OfferSerializer(offerDetails)
        offerBrief = json.loads(json.dumps(offerDataSerializer.data))
        offersObject = OffersBrief(offerBrief['id'], offerBrief['StoreName'], [offerBrief['StoreType']],
                                   offerBrief['StoreInCharge'], offerBrief['Description'],
                                   offerBrief['Image'], offerBrief['StorePara'], offerBrief['StoreMap'])
        offersObject.__dict__['couponCode'] = couponCode
        offersObject.__dict__['doneBy'] = userData['Name']
        offersObject.__dict__['timeStamp'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        if request.session.has_key('logged_in'):
            offersObject.__dict__['logged_in'] = "true"
        elif request.session.has_key('superAdmin'):
            offersObject.__dict__["superAdmin"] = "true"
        offerJson = json.dumps(offersObject.__dict__)
        couponList.append(offersObject.__dict__)
        models.User.objects.filter(UserName=username).update(Coupons=json.dumps(couponList))
        messages.success(request, "Thank you for the purchase, your coupon code is given below")
        return render(request, 'couponOffer.html', {'data': json.loads(offerJson)})
    except:
        messages.error(request, "Something went wrong,Please try again later")
    return redirect("/")


def couponHistory(request):
    try:
        coupon = {}
        if request.session.has_key('logged_in'):
            coupon = {'logged_in': "true"}
        userDetails = models.User.objects.get(UserName=request.session.get("userName"))
        userDataSerializer = serializer.UserSerializer(userDetails)
        userData = json.loads(json.dumps(userDataSerializer.data))
        if userData['Coupons'] is not None:
            couponDetails = json.loads(userData['Coupons'])
            if couponDetails is not None:
                coupon["coupon_list"] = couponDetails
        else:
            messages.info(request, "No Coupon History Found")
            return redirect("/")
        return render(request, 'couponHistory.html', {'data': coupon})
    except:
        messages.error(request, "Something went wrong, Please try again later")
        return redirect("/")
