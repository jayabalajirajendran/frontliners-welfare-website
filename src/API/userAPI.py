from django.shortcuts import render, redirect
from django.contrib import messages
from src.database import databaseQuery
from src.Model.UserDetails import UserDetails
from config import common
from webapp import forms
from webapp import models
from src.serializers import serializer
from rest_framework.utils import json


def createUser(request):
    form = forms.CreateUserForm(request.POST)
    if request.method == "POST":
        user_data = None
        try:
            userDetails = models.User.objects.all()
            userDataSerializer = serializer.UserSerializer(userDetails, many=True)
            user_data = json.loads(json.dumps(userDataSerializer.data))
        except models.User.DoesNotExist:
            userDetails = None
        userName = form.data.get('userName')
        email = form.data.get('email')
        if user_data and userDetails:
            for userData in user_data:
                userDetails = UserDetails(userData['id'], userData['Name'], userData['UserName'], userData['Password'],
                                          userData['Email'],
                                          userData['Gender'], userData['DateOfBirth'],
                                          userData['PhoneNumber'],
                                          userData['Coupons'])
                if userName == userDetails.getUserName():
                    messages.error(request, "User Name Already Taken, Please try with a different user name")
                    return redirect("/login")
        gender = form.data.get('gender')
        date_of_birth = form.data.get('dateOfBirth')
        name = form.data.get('name')
        phoneNumber = form.data.get('phoneNumber')
        password = form.data.get('password')
        databaseQuery.createUser(name.capitalize(), userName, common.hash_password(password), email, gender,
                                 date_of_birth,
                                 phoneNumber, None)
        messages.success(request, "User Account Created Successfully")
        return redirect("/login")
    return render(request, "user.html", {'form': form})
