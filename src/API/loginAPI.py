from django.shortcuts import render, redirect
from django.contrib import messages
from src.Model.UserDetails import UserDetails
from config import common
from webapp import forms
from webapp import models
from src.serializers import serializer
from rest_framework.utils import json


def login(request):
    try:
        form = forms.LoginForm(request.POST)
        if request.method == 'POST':
            userName = form.data.get('username')
            password = form.data.get('password')
            postAction = form.data.get('postAction')
            userData = None
            if postAction == 'create':
                return redirect("/create")
            try:
                userDetails = models.User.objects.get(UserName=userName)
                userDataSerializer = serializer.UserSerializer(userDetails)
                userData = json.loads(json.dumps(userDataSerializer.data))
            except models.User.DoesNotExist:
                userDetails = None
            if userDetails and userData:
                # Based on Database Details, Passing it in UserDetails Class
                userDetails = UserDetails(userData['id'], userData['Name'], userData['UserName'],
                                          userData['Password'],
                                          userData['Email'],
                                          userData['Gender'], userData['DateOfBirth'],
                                          userData['PhoneNumber'], userData['Coupons'])
                if common.verify_password(userDetails.getPassword(), password):
                    if userName == 'admin' and password == 'admin':
                        request.session['superAdmin'] = True
                    else:
                        request.session['logged_in'] = True
                    request.session['userName'] = userName
                    return redirect("/")
                else:
                    messages.error(request, "Please enter a valid password")
                    return redirect("/login")
            else:
                messages.error(request, "No Username and Password found in our records, Please try again")
                return redirect("/login")
        return render(request, 'login.html', {'form': form})
    except:
        messages.error(request, "Something went wrong, Please try again later")
        return redirect("/")


def logout(request):
    try:
        if request.session.has_key('superAdmin'):
            del request.session['superAdmin']
        else:
            del request.session['logged_in']
        return redirect("/")
    except:
        messages.error(request, "Something went wrong,Please try again later")
        return redirect("/")
