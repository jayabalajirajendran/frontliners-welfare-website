from django.shortcuts import render, redirect
from django.contrib import messages
from src.database import databaseQuery
from src.Model.UserDetails import UserDetails
from src.Model.OffersBrief import OffersBrief
from webapp import forms
from webapp import models
from src.serializers import serializer
from rest_framework.utils import json


def admin(request):
    try:
        dataRecords = {}
        userDetails = models.User.objects.all()
        userDataSerializer = serializer.UserSerializer(userDetails, many=True)
        user_data = json.loads(json.dumps(userDataSerializer.data))
        offerDetails = models.Offers.objects.all()
        offerDataSerializer = serializer.OfferSerializer(offerDetails, many=True)
        offer_data = json.loads(json.dumps(offerDataSerializer.data))
        offerList = []
        userList = []
        couponList = []
        request.session["superAdmin"] = True
        for userData in user_data:
            userDetails = UserDetails(userData['id'], userData['Name'], userData['UserName'], userData['Password'],
                                      userData['Email'],
                                      userData['Gender'], userData['DateOfBirth'],
                                      userData['PhoneNumber'],
                                      userData['Coupons'])
            if userDetails.getUserName() is not "admin":
                jsonDump = json.dumps(userDetails.__dict__)
                userList.append(json.loads(jsonDump))
            if userDetails.getCoupons() is not None:
                couponList.append(json.loads(userData['Coupons']))
        for offerBrief in offer_data:
            offersObject = OffersBrief(offerBrief['id'], offerBrief['StoreName'], [offerBrief['StoreType']],
                                       offerBrief['StoreInCharge'], offerBrief['Description'],
                                       offerBrief['Image'], offerBrief['StorePara'], offerBrief['StoreMap'])
            jsonDump = json.dumps(offersObject.__dict__)
            offerList.append(json.loads(jsonDump))
        dataRecords['offerList'] = offerList
        dataRecords['userList'] = userList
        dataRecords['couponList'] = couponList
        return render(request, 'admin.html', {'data': json.dumps(dataRecords)})
    except:
        messages.error(request, "Something went wrong, Please try again later")
        return redirect("/")


def addStore(request):
    try:
        form = forms.AddStoreForm(request.POST)
        if request.method == "POST":
            postAction = form.data.get("postAction")
            if postAction == 'cancel':
                return redirect("/")
            storeName = form.data.get("storeName")
            storeInCharge = form.data.get("storeInCharge")
            storeType = form.data.get("storeType")
            description = form.data.get("description")
            image = form.data.get("image")
            storePara = form.data.get("storePara")
            storeMap = form.data.get("storeMap")
            databaseQuery.addStore(storeName, storeType, storeInCharge.title(),
                                   description, image, storePara, storeMap)
            messages.success(request, "Store Added Successfully")
            return redirect("/")
        return render(request, 'add.html', {'form': form})
    except:
        messages.error(request, "Something went wrong,Please try again later")
        return redirect("/")


def updateStore(request):
    try:
        form = forms.UpdateStoreForm(request.POST)
        offersDictionary = {}
        storeId = request.GET.get('id')
        offerDetails = models.Offers.objects.all()
        offerDataSerializer = serializer.OfferSerializer(offerDetails, many=True)
        offer_data = json.loads(json.dumps(offerDataSerializer.data))
        if storeId is not None:
            for offerBrief in offer_data:
                offersObject = OffersBrief(offerBrief['id'], offerBrief['StoreName'], [offerBrief['StoreType']],
                                           offerBrief['StoreInCharge'], offerBrief['Description'],
                                           offerBrief['Image'], offerBrief['StorePara'], offerBrief['StoreMap'])
                if storeId == str(offersObject.getStoreId()):
                    offersDictionary['offersList'] = offersObject.__dict__
        else:
            if request.method == "POST":
                postAction = form.data.get("postAction")
                storeId = int(form.data.get("storeId"))
                if postAction == 'delete':
                    models.Offers.objects.filter(id=storeId).delete()
                    messages.success(request, "Store Deleted Successfully")
                    return redirect("/")
                storeName = form.data.get("storeName")
                storeInCharge = form.data.get("storeInCharge")
                storeType = form.data.get("storeType")
                description = form.data.get("description")
                storePara = form.data.get("storePara")
                image = form.data.get("image")
                storeMap = form.data.get("storeMap")
                models.Offers.objects.filter(id=storeId).update(StoreName=storeName, StoreInCharge=storeInCharge,
                                                                StoreType=storeType, Description=description,
                                                                StorePara=storePara, Image=image, StoreMap=storeMap)
                messages.success(request, "Store Updated Successfully")
                return redirect("/")
        return render(request, 'update.html', {'form': form, 'update': offersDictionary})
    except:
        messages.error(request, "Something went wrong,Please try again later")
        return redirect("/")
