from django.shortcuts import render, redirect
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from rest_framework.utils import json
from src.connection import HttpRestAPI
from src.API import loginAPI
from src.API import newAPI
from src.API import offerAPI
from src.API import adminAPI
from src.API import userAPI


# Home Page
@csrf_exempt
def home(request):
    try:
        data = {}
        if request.session.has_key('logged_in'):
            data['logged_in'] = "true"
        elif request.session.has_key('superAdmin'):
            data["superAdmin"] = "true"
        apiResponse = []
        coronaSummaryAPI = HttpRestAPI.getCoronaAPISummaryResponse()
        coronaRecentCasesAPI = HttpRestAPI.getCoronaCasesIrelandResponse()
        newsAPI = HttpRestAPI.getNewsAPIResponse()
        data['coronaSummaryResponse'] = json.loads(coronaSummaryAPI)
        data['coronaRecentCases'] = json.loads(coronaRecentCasesAPI)
        data['newsAPI'] = json.loads(newsAPI)
        apiResponse.append(data)
        return render(request, 'index.html',
                      {'data': apiResponse})
    except:
        messages.error(request, "Something went wrong, Please try again later")
        return redirect("/")


# News Brief Page
@csrf_exempt
def newsBrief(request):
    return newAPI.newBrief(request)


# Login Page
@csrf_exempt
def login(request):
    return loginAPI.login(request)


# Create User Page
@csrf_exempt
def createUser(request):
    return userAPI.createUser(request)


# Offers Page
@csrf_exempt
def offers(request):
    return offerAPI.offers(request)


# Store Brief Page
@csrf_exempt
def storeBrief(request):
    return offerAPI.storeBrief(request)


# Add Offer Store Page
@csrf_exempt
def addStore(request):
    return adminAPI.addStore(request)


# Update Offer Store Page
@csrf_exempt
def updateStore(request):
    return adminAPI.updateStore(request)


# Purchasing Coupon Offer and Final Confirmation Page
@csrf_exempt
def couponOffer(request):
    return offerAPI.couponOffer(request)


# Coupon History Page
@csrf_exempt
def couponHistory(request):
    return offerAPI.couponHistory(request)


# Admin Page
@csrf_exempt
def admin(request):
    return adminAPI.admin(request)


# Logout Page
@csrf_exempt
def logout(request):
    return loginAPI.logout(request)
