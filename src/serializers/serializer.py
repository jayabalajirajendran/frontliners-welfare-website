from rest_framework import serializers
from webapp.models import User
from webapp.models import Offers


# Convert User model data to JSON using Serializers
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


# Convert Offer model to JSON using Serializers
class OfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offers
        fields = '__all__'
