import requests
from rest_framework.utils import json
from config import common


# Get Corona Summary from API
def getCoronaAPISummaryResponse():
    response = requests.get('https://api.covid19api.com/summary')
    if response.status_code == 200 and common.FETCH_FROM_API:
        return json.dumps(response.json()['Global'])
    else:
        with open(
                common.RESOURCE_FOLDER + '/json/CoronaCasesSummary.json') as json_file:
            data = json.load(json_file)
        return json.dumps(data)


# Get Corona Cases in Ireland from API
def getCoronaCasesIrelandResponse():
    response = requests.get('https://api.covid19api.com/total/country/ireland/status/confirmed')
    if response.status_code == 200 and common.FETCH_FROM_API:
        return json.dumps(response.json())
    else:
        with open(
                common.RESOURCE_FOLDER + '/json/CoronaCasesIrelandResponse.json') as json_file:
            data = json.load(json_file)
        return json.dumps(data)


# Get Latest Headlines published in RTE and Irish Times from API
def getNewsAPIResponse():
    newsAPI = requests.get(
        ('http://newsapi.org/v2/top-headlines?sources=' + str({0}) + '&apiKey=' + str({1}) + '').format(
            common.NEWS_SOURCE,
            common.API_KEY))
    response = json.loads(newsAPI.text)
    if response is not None and common.FETCH_FROM_API:
        return json.dumps(response['articles'][:8])
    else:
        with open(
                common.RESOURCE_FOLDER + '/json/NewsApiResponse.json') as json_file:
            data = json.load(json_file)
    return json.dumps(data['articles'][:8])
