"""DBS_Welfare_Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/url/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.url import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.url'))
"""
# from django.contrib import admin
from django.urls import path
from src.views.views import home
from src.views.views import newsBrief
from src.views.views import login
from src.views.views import createUser
from src.views.views import logout
from src.views.views import offers
from src.views.views import storeBrief
from src.views.views import addStore
from src.views.views import updateStore
from src.views.views import couponOffer
from src.views.views import couponHistory
from src.views.views import admin

urlpatterns = [
    path('', home),
    path('newsBrief', newsBrief),
    path('login', login),
    path('create', createUser),
    path('logout', logout),
    path('offers', offers),
    path('storeBrief', storeBrief),
    path('add', addStore),
    path('update', updateStore),
    path('couponOffer', couponOffer),
    path('coupons', couponHistory),
    path('admin', admin)
]
