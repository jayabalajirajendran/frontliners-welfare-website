from webapp import models


# Create User in database
def createUser(name, username, password, email, gender, dateOfBirth, phoneNumber, coupons):
    createUserDetails = models.User(Name=name, UserName=username, Password=password, Email=email, Gender=gender,
                                    DateOfBirth=dateOfBirth, PhoneNumber=phoneNumber, Coupons=coupons)
    createUserDetails.save()


# Add Store in database
def addStore(storeName, storeType, storeInCharge, description, image, storePara, storeMap):
    addStoreDetails = models.Offers(StoreName=storeName, StoreType=storeType, StoreInCharge=storeInCharge,
                                    Description=description, Image=image,
                                    StorePara=storePara, StoreMap=storeMap)
    addStoreDetails.save()
