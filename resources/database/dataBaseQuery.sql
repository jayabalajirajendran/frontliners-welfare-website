-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: localhost    Database: Welfare
-- ------------------------------------------------------
-- Server version	5.7.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;


# Adding Admin to the project
INSERT INTO webapp_user(Name, UserName, Password, Email, Gender, DateOfBirth, PhoneNumber)
VALUES ('admin', 'admin',
        'fc85b94495a67bb6549b575ee3c904905e152bdf9956dc6163f9b3037f25f82e7bef0cd046dbcd452a95f086bda056db7aa3ab95ea7dce397ba211cfc4f0e38eeeed8f0a1be7f2a03a4116f277be283e5b94b36755175eb5cd268adda0475e43',
        'admin@gmail.com', 'male', '1995-05-20', '9789097890');

# Adding store offers to the project
INSERT INTO webapp_offers (StoreType, StoreName, StoreInCharge, Description, Image, StorePara, StoreMap)
VALUES ('Restaurant', 'Four Star Pizza', 'John Mac',
        'To bring you a truly authentic pizza experience our dough is still freshly made every day in each of our stores',
        'https://just-eat-prod-eu-res.cloudinary.com/image/upload/c_fill,d_ie:cuisines:pizza-4.jpg,f_auto,q_auto,w_500/v1/ie/restaurants/504.jpg',
        'Four Star Pizza is a fast food pizza company which operates throughout the island of Ireland. It originated in Washington, Pennsylvania, US, in 1981 by Alan and Susan Cottrill, who previously operated Dominos Pizza stores in Ohio.',
        'https://www.google.com/maps/dir//four+star+pizza/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x48670c45231dbf31:0x6b54a1629e8bd117?sa=X&ved=2ahUKEwjo8e_Eu8bqAhWRWhUIHWnYBVQQ9RcwAHoECAcQCQ');
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

