/*  Main JS*/


(function ($) {


        $WIN = $(window);
        /* Preloader will fade out the loading animation*/
        var ssPreloader = function () {
            $WIN.on('load', function () {
                $("#loader").fadeOut("slow", function () {
                    $("#preloader").delay(300).fadeOut("slow");

                });
            });
        };

        function unique(list) {
            var result = [];
            $.each(list, function (i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }


        if (typeof update != "undefined" && update != null) {
            const storeToUpdate = update['offersList'];
            $('#id_storeId').val(storeToUpdate["id"]);
            $('#id_storeName').val(storeToUpdate["storeName"]);
            $('#id_storeInCharge').val(storeToUpdate["storeInCharge"]);
            $('#id_storeType').val(storeToUpdate["storeType"]);
            $('#id_description').val(storeToUpdate["description"]);
            $('#id_storePara').val(storeToUpdate["storePara"]);
            $('#id_image').val(storeToUpdate["image"]);
            $('#id_storeMap').val(storeToUpdate["storeMap"]);
        }

        /* Mobile Menu*/
        var ssMobileNav = function () {

            var toggleButton = $('.menu-toggle'),
                nav = $('.main-navigation');

            toggleButton.on('click', function (event) {
                event.preventDefault();

                toggleButton.toggleClass('is-clicked');
                nav.slideToggle();
            });

            if (toggleButton.is(':visible')) nav.addClass('mobile');

            $WIN.resize(function () {
                if (toggleButton.is(':visible')) nav.addClass('mobile');
                else nav.removeClass('mobile');
            });

            $('#main-nav-wrap li a').on("click", function () {
                if (nav.hasClass('mobile')) {
                    toggleButton.toggleClass('is-clicked');
                    nav.fadeOut();
                }
            });

        };
        /*	Masonry*/
        var ssMasonryFolio = function () {
            var containerBricks = $('.bricks-wrapper');

            containerBricks.imagesLoaded(function () {

                containerBricks.masonry({
                    itemSelector: '.entry',
                    columnWidth: '.grid-sizer',
                    percentPosition: true,
                    resize: true
                });

            });
        };

        /* Smooth Scrolling */
        var ssSmoothScroll = function () {

            $('.smoothscroll').on('click', function (e) {
                var target = this.hash,
                    $target = $(target);

                e.preventDefault();
                e.stopPropagation();

                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top
                }, 800, 'swing').promise().done(function () {

                    // check if menu is open
                    if ($('body').hasClass('menu-is-open')) {
                        $('#header-menu-trigger').trigger('click');
                    }

                    window.location.hash = target;
                });
            });

        };


        /* Placeholder Plugin Settings */
        var ssPlaceholder = function () {
            $('input, textarea, select').placeholder();
        };


        /* Back to Top */
        var ssBackToTop = function () {

            var pxShow = 500,
                fadeInTime = 400,
                fadeOutTime = 400,
                goTopButton = $("#go-top");
            $(window).on('scroll', function () {
                if ($(window).scrollTop() >= pxShow) {
                    goTopButton.fadeIn(fadeInTime);
                } else {
                    goTopButton.fadeOut(fadeOutTime);
                }
            });
        };


        /* Initialize */
        (function ssInit() {
            ssSmoothScroll();
            ssPreloader();
            ssMobileNav();
            ssMasonryFolio();
            ssPlaceholder();
            ssBackToTop();
        })();
    }
)(jQuery);

/*---------Shuffle.Js-----------------*/
var Shuffle = window.Shuffle;

class Demo {
    constructor(element) {
        this.element = element;
        this.shuffle = new Shuffle(document.getElementsByClassName('bricks-wrapper')[0], {
            itemSelector: '.brick'
        });

        // Log events.
        this.addShuffleEventListeners();
        this.addFilterButtons();
        this.addSearchFilter();
    }

    /**
     * Shuffle uses the CustomEvent constructor to dispatch events. You can listen
     * for them like you normally would (with jQuery for example).
     */
    addShuffleEventListeners() {
        this.shuffle.on(Shuffle.EventType.LAYOUT, (data) => {
            console.log('layout. data:', data);
        });
        this.shuffle.on(Shuffle.EventType.REMOVED, (data) => {
            console.log('removed. data:', data);
        });
    }

    addFilterButtons() {
        const author = document.querySelector('.author-filter-options');
        if (!author) {
            return;
        }
        const authorFilterButtons = Array.from(author.children);
        const onClick = this._handleFilterClick.bind(this);
        authorFilterButtons.forEach((button) => {
            button.addEventListener('click', onClick, false);
        });
    }

    _handleFilterClick(evt) {
        const btn = evt.currentTarget;
        const isActive = btn.classList.contains('active');
        const authorBtn = btn.getAttribute('data-group');

        this._removeActiveClassFromChildren(btn.parentNode);

        let filterGroup;
        if (isActive) {
            btn.classList.remove('active');
            filterGroup = Shuffle.ALL_ITEMS;
        } else {
            btn.classList.add('active');
            filterGroup = authorBtn;
        }
        this.shuffle.filter(filterGroup)
    }

    _removeActiveClassFromChildren(parent) {
        const {children} = parent;
        for (let i = children.length - 1; i >= 0; i--) {
            children[i].classList.remove('active');
        }
    }

    addSearchFilter() {
        const searchInput = document.querySelector('.category-filter-options');
        if (!searchInput) {
            return;
        }
        searchInput.addEventListener('keyup', this._handleSearchKeyup.bind(this));
    }

    /**
     * Filter the shuffle instance by items with a title that matches the search input.
     * @param {Event} evt Event object.
     */
    _handleSearchKeyup(evt) {
        const searchText = evt.target.value.toLowerCase();
        this.shuffle.filter((element, shuffle) => {
            // If there is a current filter applied, ignore elements that don't match it.
            if (shuffle.group !== Shuffle.ALL_ITEMS) {
                // Get the item's groups.
                const author = JSON.parse(element.getAttribute('data-groups').toLowerCase().trim())[0];
                if (shuffle.group.toLowerCase() === author) {
                    const category = JSON.parse(element.getAttribute('data-search').toLowerCase().trim());
                    const isElementInCurrentGroup = category.indexOf(searchText) !== -1;
                    // Only search elements in the current group
                    if (!isElementInCurrentGroup) {
                        return false;
                    }
                    return isElementInCurrentGroup;
                }
                return false;
            }
            const titleElement = element.querySelector('.picture-item__title');
            const titleText = titleElement.textContent.toLowerCase().trim();
            return titleText.indexOf(searchText) !== -1;
        });
    }

}


document.addEventListener('DOMContentLoaded', () => {
    setTimeout(() => {
        window.demo = new Demo(document.getElementById('brick_wrapper_format'));
    }, 1000);

});


