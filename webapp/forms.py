from django import forms


# Forms for Login HTML Page
class LoginForm(forms.Form):
    username = forms.CharField(required=False, label='username', max_length=100,
                               widget=forms.TextInput(attrs={'type': 'text', 'name': 'username'}))
    password = forms.CharField(required=False, label='password', max_length=100,
                               widget=forms.TextInput(
                                   attrs={'type': 'password', 'name': 'password'}))


# Forms for User HTML Page
class CreateUserForm(forms.Form):
    name = forms.CharField(required=True, label='Name', max_length=100,
                           widget=forms.TextInput(attrs={'type': 'text', 'name': 'name'}))
    email = forms.CharField(required=True, label='Email', max_length=100,
                            widget=forms.TextInput(
                                attrs={'type': 'email', 'name': 'email', 'onchange': 'email_validate(this.value);'}))
    gender = forms.CharField(required=True, label='Gender', max_length=100,
                             widget=forms.TextInput(
                                 attrs={'type': 'text', 'name': 'gender'}))
    dateOfBirth = forms.CharField(required=False, label='DateOfBirth', max_length=100,
                                  widget=forms.TextInput(
                                      attrs={'type': 'date', 'name': 'dateOfBirth'}))
    phoneNumber = forms.CharField(required=False, label='PhoneNumber', max_length=15,
                                  widget=forms.TextInput(
                                      attrs={'type': 'text', 'name': 'phoneNumber', 'onkeyup': 'validatephone(this);'}))
    userName = forms.CharField(required=False, label='UserName', max_length=100,
                               widget=forms.TextInput(
                                   attrs={'type': 'text', 'name': 'userName'}))
    password = forms.CharField(required=True, label='Password', max_length=16, min_length="4",
                               widget=forms.TextInput(
                                   attrs={'type': 'password', 'name': 'password', 'id': 'pass1'}))

    confirmPassword = forms.CharField(required=True, label='ConfirmPassword', max_length=16, min_length="4",
                                      widget=forms.TextInput(
                                          attrs={'type': 'password', 'name': 'confirm_password', 'id': 'pass2',
                                                 'onkeyup': 'checkPass();'}))


# Forms for Adding Store in Add HTML Page
class AddStoreForm(forms.Form):
    storeName = forms.CharField(required=True, label='StoreName', max_length=100,
                                widget=forms.TextInput(attrs={'type': 'text', 'name': 'storeName'}))
    storeInCharge = forms.CharField(required=True, label='StoreInCharge', max_length=100,
                                    widget=forms.TextInput(
                                        attrs={'type': 'text', 'name': 'storeInCharge'}))
    storeType = forms.CharField(required=True, label='StoreType', max_length=100,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'name': 'storeType'}))
    description = forms.CharField(required=True, label='description',
                                  widget=forms.TextInput(
                                      attrs={'type': 'text', 'name': 'description'}))
    image = forms.CharField(required=True, label='Image',
                            widget=forms.TextInput(
                                attrs={'type': 'text', 'name': 'image'}))

    storePara = forms.CharField(required=True, label='StoryPara',
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'name': 'storePara'}))
    storeMap = forms.CharField(required=True, label='StoreMap', max_length=100,
                               widget=forms.TextInput(
                                   attrs={'type': 'text', 'name': 'storeMap'}))


# Forms for Updating Store in Update HTML Page
class UpdateStoreForm(forms.Form):
    storeId = forms.CharField(label='StoreId', max_length=100,
                              widget=forms.TextInput(attrs={'type': 'text', 'name': 'storeId', 'readonly': 'readonly'}))
    storeName = forms.CharField(required=True, label='StoreName', max_length=100,
                                widget=forms.TextInput(attrs={'type': 'text', 'name': 'storeName'}))
    storeInCharge = forms.CharField(required=True, label='StoreInCharge', max_length=100,
                                    widget=forms.TextInput(
                                        attrs={'type': 'text', 'name': 'storeInCharge'}))
    storeType = forms.CharField(required=True, label='StoreType', max_length=100,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'name': 'storeType'}))
    description = forms.CharField(required=True, label='description',
                                  widget=forms.TextInput(
                                      attrs={'type': 'text', 'name': 'description'}))
    image = forms.CharField(required=True, label='Image',
                            widget=forms.TextInput(
                                attrs={'type': 'text', 'name': 'image'}))

    storePara = forms.CharField(required=True, label='StoryPara',
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'name': 'storePara'}))
    storeMap = forms.CharField(required=True, label='StoreMap', max_length=100,
                               widget=forms.TextInput(
                                   attrs={'type': 'text', 'name': 'storeMap'}))
