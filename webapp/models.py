from django.db import models


# User Model Class for Database
class User(models.Model):
    Name = models.TextField(max_length=100)
    UserName = models.TextField(max_length=100)
    Password = models.TextField(max_length=100)
    Email = models.TextField(max_length=100)
    Gender = models.TextField(max_length=100)
    DateOfBirth = models.TextField(max_length=100)
    PhoneNumber = models.TextField(max_length=100)
    Coupons = models.TextField(null=True, max_length=100)

    def __str__(self):
        return self.Name


# Offer Model Class for Database
class Offers(models.Model):
    StoreType = models.TextField(max_length=100)
    StoreName = models.TextField(max_length=100)
    StoreInCharge = models.TextField(max_length=100)
    Description = models.TextField(max_length=500)
    Image = models.TextField(max_length=100)
    StorePara = models.TextField(max_length=100)
    StoreMap = models.TextField(max_length=100)

    def __str__(self):
        return self.StoreName
