from django.template.defaulttags import register


# Custom Tags for getting Item from List in HTML
@register.filter
def getItemFromList(list, key):
    return list[0].get(key)


# Custom Tags for getting Item a Dictionary in HTML
@register.filter
def getItem(list, key):
    return list.get(key)
