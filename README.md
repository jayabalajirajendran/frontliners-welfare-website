**Scenario:**<br>
    Develop a web app on a topic of your choice using supportive frameworks and technologies.
The aim of the project is to test the student's ability to research, design, implement and report on
relevant technologies related to Web Development for Information Systems. The project must involve
a complete IS solution in the form of a Web App or proof of concept system.

Subject areas for this project cover a wide variety of areas, some covered in class, others requiring
independent research. Applicable system administration technologies are:

● Use of a modern framework for interactive front-end showing evidence of user-experience
design<br>
● Suitable back-end technologies (relational or non-relational)<br>
● Consumption of web services<br>
● Technologies to counter security vulnerabilities<br>
● Use of JSON/XML<br>
● Design Patterns<br>
● Deployment to Cloud<br>

**NOTE**: The whole project is completely done by myself, Jayabalaji Rajendran(10537723)

**PROOF OF CONCEPT:**

Creating a coupon offer listing website integrated with news and corona api for the live feed
that allows the frontliners to view the current headlines and offers specifically for the frontliners provided by restaurants,
cafe and many more.

**TECHNOLOGIES USED:**

● MYSQL Database for Backend Integration<br>
● Python Programming using Django For API Integration<br>
● HTML,CSS, jQuery, JavaScript for Front End Integration using Jinja2.<br>
● Bootstrap.js for displaying carousel in Home Page.<br>
● Chart.js for displaying charts and statistics.(trail version used)<br>
● Shuffle.js for sorting and displaying Offers.<br>

**HOW TO RUN THE PROJECT:**

**NOTE:** Make sure you changed the details in common.py file in config folder to your local machine path and other configuration <br>

**NOTE:** Please check the database query in resource folder for adding admin to this project. <br>

**STEP 1:**
 Install the News API package by using below command <br> <br>
 
 **pip install newsapi-python**
 
 **STEP 2:**
  Install all the packages mentioned in requirements.txt file by using below command <br> <br>
  
  **pip install -r requirements.txt**
  
  **NOTE:** If any error comes in as "mysql config not found" then , run this command "sudo apt-get install libmysqlclient-dev" 
  and try running the requirements.txt again.
  
  **STEP 3:**
  Once all the packages installed, create a database "Welfare" which I used as database name and run this command to start migration <br><br>
 **python manage.py migrate**<br>
 **python manage.py makemigrations webapp**<br>
 **python manage.py migrate webapp**<br>
 
 **STEP 4:**
   To the final process, Run the Django webapp using the command runserver. <br><br>
   **python manage.py runserver**
   
   Thank you for your time, Have a great day
